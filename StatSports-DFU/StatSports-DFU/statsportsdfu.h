#pragma once

#include <QtWidgets/QWidget>
#include "statsportsdfu.h"
#include "ui_statsportsdfu.h"
#include "DownloadManager\DownloadManager_h\DownloadManager.h"
#include "DownloadManager\DownloadManager_h\Constants.h"

class StatSportsDFU : public QWidget, public IDownloadListener
{
	Q_OBJECT

public slots:
  void on_downloadButton_clicked();

public:
	StatSportsDFU(QWidget *parent = Q_NULLPTR);

	void IDownloadListener::OnStart();
	void IDownloadListener::OnUpdate(ULONG u_value, ULONG u_max_value);
	void IDownloadListener::OnStop();

	DownloadManager* _downloadManager;
	Constants* _constants;

private:
	Ui::StatSportsDFUClass _ui;
};
