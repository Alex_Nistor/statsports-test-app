#include "stdafx.h"
#include "DownloadManager\DownloadManager_h\DownloadThread.h"

DownloadThread::DownloadThread(IBindStatusCallback* call_back)
{
	_callBack = call_back;
}

DownloadThread::~DownloadThread()
{
}

void DownloadThread::run()
{
	URLDownloadToFile(0, _downloadUrl.c_str(), _localPath.c_str(), 0, static_cast<IBindStatusCallback*>(_callBack));
}

std::wstring DownloadThread::ConvertStringToLPCWSTR(const std::string & s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}
