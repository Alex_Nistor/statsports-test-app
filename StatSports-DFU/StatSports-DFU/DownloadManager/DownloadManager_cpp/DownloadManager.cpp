#include "stdafx.h"
#include "DownloadManager\DownloadManager_h\DownloadManager.h"

DownloadManager::DownloadManager()
{
	_downloadThread = new DownloadThread(this);
}

DownloadManager::~DownloadManager()
{
}

STDMETHODIMP DownloadManager::OnProgress(ULONG ulProgress, ULONG ulPRogressMAx, ULONG ulStatusCode, LPCWSTR wszStatusText)
{
	_downloadListener->OnUpdate(ulProgress, ulPRogressMAx);
	return S_OK;
}

STDMETHODIMP DownloadManager::OnStartBinding(DWORD dwReserved, IBinding * pib)
{
	_downloadListener->OnStart();
	return S_OK;
}

STDMETHODIMP_(ULONG) DownloadManager::Release()
{
	return S_OK;
}

void DownloadManager::StartDownload(std::string dw_url, std::string local_path)
{
	_downloadThread->_downloadUrl = DownloadThread::ConvertStringToLPCWSTR(dw_url);
	_downloadThread->_localPath = DownloadThread::ConvertStringToLPCWSTR(local_path);
	_downloadThread->start();
}

void DownloadManager::RegisterListener(IDownloadListener* download_listener)
{
	_downloadListener = download_listener;
}

STDMETHODIMP DownloadManager::GetPriority(LONG * pnPriority)
{
	return E_NOTIMPL;
}

STDMETHODIMP DownloadManager::OnLowResource(DWORD reserved)
{
	return E_NOTIMPL;
}

STDMETHODIMP DownloadManager::OnStopBinding(HRESULT hresult, LPCWSTR szError)
{
	return E_NOTIMPL;
}

STDMETHODIMP DownloadManager::GetBindInfo(DWORD * grfBINDF, BINDINFO * pbindinfo)
{
	return E_NOTIMPL;
}

STDMETHODIMP DownloadManager::OnDataAvailable(DWORD grfBSCF, DWORD dwSize, FORMATETC * pformatetc, STGMEDIUM * pstgmed)
{
	return E_NOTIMPL;
}

STDMETHODIMP DownloadManager::OnObjectAvailable(REFIID riid, IUnknown * punk)
{
	return E_NOTIMPL;
}

STDMETHODIMP_(ULONG) DownloadManager::AddRef()
{
	return 0;
}

STDMETHODIMP DownloadManager::QueryInterface(REFIID riid, void ** ppvObject)
{
	return E_NOTIMPL;
}
