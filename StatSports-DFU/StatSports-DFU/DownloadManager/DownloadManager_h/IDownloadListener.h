#pragma once
__interface IDownloadListener
{
public:
	/// <summary> This method gets called when the download starts </summary>
	void OnStart();

	/// <summary> This method gets called on download progress </summary>
	/// <param name="bar"> u_value shows the value of download progress, u_max_value is the max value of progress (usually 100%) </param>
	void OnUpdate(ULONG u_value, ULONG u_max_value);

	///<summary>This method gets called when the download stops </summary>
	void OnStop();
};
