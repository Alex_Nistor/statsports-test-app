#pragma once
#include "iostream"
#include <shlobj.h>
#include <string>
#include <sstream>
#include <stdlib.h>     

class Constants
{
public:
	inline std::string ReWriteDefaultPath()
	{
		wchar_t* localAppData = new wchar_t[128];

		HRESULT result = SHGetKnownFolderPath(FOLDERID_Downloads, 0, NULL, &localAppData);

		std::wstring newLocalAppData(localAppData);
		std::string finalString(newLocalAppData.begin(), newLocalAppData.end());

		return finalString;
	}

	const std::string GetDefaultSavePath() const { return kdefault_save_path; }

	const std::string kdefault_download_link = "https://wallpaperbrowse.com/media/images/3848765-wallpaper-images-download.jpg";

private:
	std::string kdefault_save_path = ReWriteDefaultPath() + "\\image.jpg";
};
