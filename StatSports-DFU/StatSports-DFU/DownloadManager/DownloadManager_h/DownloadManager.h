#pragma once
#include <QtWidgets/QWidget>
#include <qmainwindow.h>
#include "DownloadManager\DownloadManager_h\IDownloadListener.h"
#include "DownloadManager\DownloadManager_h\DownloadThread.h"

class DownloadManager : public IBindStatusCallback, public  QWidget
{
public:
	DownloadManager();
	~DownloadManager();

	STDMETHOD(OnProgress)(ULONG ulProgress, ULONG ulPRogressMAx, ULONG ulStatusCode, LPCWSTR wszStatusText);
	STDMETHOD_(ULONG, Release)();
	STDMETHOD(OnStartBinding)(DWORD dwReserved, IBinding __RPC_FAR *pib);

	void StartDownload(std::string dw_url, std::string local_path);
	void RegisterListener(IDownloadListener* download_listener);

	STDMETHOD(GetPriority)(LONG __RPC_FAR *pnPriority);
	STDMETHOD(OnLowResource)(DWORD reserved);
	STDMETHOD(OnStopBinding)(HRESULT hresult, LPCWSTR szError);
	STDMETHOD(GetBindInfo)(DWORD __RPC_FAR *grfBINDF, BINDINFO __RPC_FAR *pbindinfo);
	STDMETHOD(OnDataAvailable)(DWORD grfBSCF, DWORD dwSize, FORMATETC __RPC_FAR *pformatetc, STGMEDIUM __RPC_FAR *pstgmed);
	STDMETHOD(OnObjectAvailable)(REFIID riid, IUnknown __RPC_FAR *punk);
	STDMETHOD_(ULONG, AddRef)();
	STDMETHOD(QueryInterface)(REFIID riid, void __RPC_FAR *__RPC_FAR *ppvObject);

private:
	IDownloadListener * _downloadListener;
	DownloadThread * _downloadThread;
};
