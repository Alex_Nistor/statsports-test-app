#pragma once
#include "urlmon.h"
#include<windows.h>
#pragma comment(lib, "urlmon.lib")
#pragma comment(lib,"wininet.lib")

class DownloadThread : public QThread
{
public:
	static std::wstring ConvertStringToLPCWSTR(const std::string& s);

	DownloadThread(IBindStatusCallback* call_back);
	~DownloadThread();

	std::wstring _downloadUrl;
	std::wstring _localPath;

protected:
	void run(); // override the run() function of QThread 

private:
	IBindStatusCallback * _callBack;
};
