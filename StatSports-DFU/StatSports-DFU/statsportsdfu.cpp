#include "stdafx.h"
#include "statsportsdfu.h"

StatSportsDFU::StatSportsDFU(QWidget *parent)
	: QWidget(parent)
{
	_ui.setupUi(this);

	_downloadManager = new DownloadManager();
	_constants = new Constants();
	_downloadManager->RegisterListener(this);
}

void StatSportsDFU::on_downloadButton_clicked()
{
	//_downloadManager->StartDownload(_constants->kdefault_download_link, _constants->GetDefaultSavePath());
}

void StatSportsDFU::OnStart()
{
	std::cout << "Checking for updates...";
}

void StatSportsDFU::OnUpdate(ULONG u_value, ULONG u_max_value)
{
	std::cout << "progress updated";
}

void StatSportsDFU::OnStop()
{
	std::cout << "Stoped download";
	// verify Firmware version from connected device with the downloaded file.
	//update label "There is an update available for your device."
	// show button "Update"
}